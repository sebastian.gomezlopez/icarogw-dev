# Icarogw installation instructions

More details [here](https://github.com/simone-mastrogiovanni/icarogw)

* Create a conda environment with `conda create -n icarogw python=3.8`

* Run `pip install git+https://github.com/simone-mastrogiovanni/icarogw.git` to install icarogw

* Run `conda install -c conda-forge cupy==12.0` if you want to install Cupy

# Get your data

Download all the data from this [folder](https://drive.google.com/drive/folders/1EVc-lKYO4KbAIyliePrfYjpTNzEy4gyb?usp=sharing). 
